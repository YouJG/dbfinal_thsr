<?php
$stationName= filter_input(INPUT_POST, 'stationID');
switch ($stationName) {
    case 1:$stationName="南港站";break;
    case 2:$stationName="台北站";break;
    case 3:$stationName="板橋站";break;
    case 4:$stationName="桃園站";break;
    case 5:$stationName="新竹站";break;
    case 6:$stationName="苗栗站";break;
    case 7:$stationName="台中站";break;
    case 8:$stationName="彰化站";break;
    case 9:$stationName="雲林站";break;
    case 10:$stationName="嘉義站";break;
    case 11:$stationName="台南站";break;
    case 12:$stationName="左營站";break;
    
}
$server=new mysqli("microcraft.nctu.me", "h24573021", "h24573021", "thsr");
mysqli_set_charset($server, "utf8");
$result=$server->query("SELECT name, address, lat, lng, imgUrl FROM station where name='$stationName'")->fetch_all();
if($result!=null){
    $stationAddress=$result[0][1];
    $stationLat=$result[0][2];
    $stationLng=$result[0][3];
    $stationImg=$result[0][4];
}
else{
    die("no way~");
}

$chartData=$server->query("select number,Year,Month from numberofpeople where Name='$stationName' ORDER by Year DESC,Month DESC LIMIT 12")->fetch_all();
$chartData= json_encode($chartData,JSON_NUMERIC_CHECK);

?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $stationName; ?> --台灣高鐵</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        
        <!--home page resource-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" type="text/css" href="style.css" />
        <script src="script.js"></script>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <style>
            #mapDiv {
                width: 500px;
                height: 342px;
            }
            #floating-panel {
                position: absolute;
                top: 10px;
                left: 25%;
                z-index: 5;
                background-color: #fff;
                padding: 5px;
                border: 1px solid #999;
                text-align: center;
                font-family: 'Roboto','sans-serif';
                line-height: 30px;
            }
        </style>
    </head>
    <body onload="chart()">
        
        <!--header-->
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div style="background-color: rgb(250,250,250);width: 100%;height: 100px;">
                        <div style="float: left;width: 100%;height: 100%;background-image: url(./hsr.png);background-repeat: no-repeat;min-width: 255px">
                            <a href="#" style="display: block;width: 100%;height: 100%;"></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-9">

                    <div class="container" style="background-color: rgb(250,250,250);">
                        <div class="row align-middle">
                            <div class="col">
                                <div class="form-group">
                                    <select class="timetable" id="original" style="min-width: 90px;">
                                        <option value="0990">南港</option>
                                        <option value="1000">台北</option>
                                        <option value="1010">板橋</option>
                                        <option value="1020">桃園</option>
                                        <option value="1030">新竹</option>
                                        <option value="1035">苗栗</option>
                                        <option value="1040">台中</option>
                                        <option value="1043">彰化</option>
                                        <option value="1047">雲林</option>
                                        <option value="1050">嘉義</option>
                                        <option value="1060">台南</option>
                                        <option value="1070">左營</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <select class="timetable" id="destnation" style="min-width: 90px;">
                                    <option value="0990">南港</option>
                                    <option value="1000" selected="selected">台北</option>
                                    <option value="1010">板橋</option>
                                    <option value="1020">桃園</option>
                                    <option value="1030">新竹</option>
                                    <option value="1035">苗栗</option>
                                    <option value="1040">台中</option>
                                    <option value="1043">彰化</option>
                                    <option value="1047">雲林</option>
                                    <option value="1050">嘉義</option>
                                    <option value="1060">台南</option>
                                    <option value="1070">左營</option>
                                </select>
                            </div>
                            <div class="col">
                                <div class="input-group inputWithIcon">
                                    <input class="timetable date" id="date" type="date" style="padding-right: 40px;max-width: 210px;">
                                    <i class="material-icons">date_range</i>
                                </div>
                            </div>
                            <div class="col">
                                <div class="input-group inputWithIcon" style="min-width: 100px;">
                                    <select class="timetable" id="time">
                                        <option value="300">05:00</option>
                                        <option value="330">05:30</option>
                                        <option value="360">06:00</option>
                                        <option value="390">06:30</option>
                                        <option value="420">07:00</option>
                                        <option value="450">07:30</option>
                                        <option value="480">08:00</option>
                                        <option value="510">08:30</option>
                                        <option value="540">09:00</option>
                                        <option value="570">09:30</option>
                                        <option value="600">10:00</option>
                                        <option value="630">10:30</option>
                                        <option value="660">11:00</option>
                                        <option value="690">11:30</option>
                                        <option value="720">12:00</option>
                                        <option value="750">12:30</option>
                                        <option value="780">13:00</option>
                                        <option value="810">13:30</option>
                                        <option value="840">14:00</option>
                                        <option value="870">14:30</option>
                                        <option value="900">15:00</option>
                                        <option value="930">15:30</option>
                                        <option value="960">16:00</option>
                                        <option value="990">16:30</option>
                                        <option value="1020">17:00</option>
                                        <option value="1050">17:30</option>
                                        <option value="1080">18:00</option>
                                        <option value="1110">18:30</option>
                                        <option value="1140">19:00</option>
                                        <option value="1170">19:30</option>
                                        <option value="1200">20:00</option>
                                        <option value="1230">20:30</option>
                                        <option value="1260">21:00</option>
                                        <option value="1290">21:30</option>
                                        <option value="1320">22:00</option>
                                        <option value="1350">22:30</option>
                                        <option value="1380">23:00</option>
                                        <option value="1410">23:30</option>
                                    </select>
                                    <i class="material-icons">alarm</i>
                                </div>
                            </div>
                            <div class="col">

                                <div id="overlay" class="overlay">
                                    <button style="border-radius:5px;color:#ffffff;background-color:rgb(255,69,0);display:inline-block;padding:5px 10px;position:absolute;cursor:pointer;" onclick="off()">X</button>
                                    <div id="text" class="text">

                                    </div>
                                </div>

                                <button class="revision04_search_btn" onclick="on()"></button>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>


        <!--navbar-->
        <nav class="navbar navbar-expand-sm navbar-dark bg-dark">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="container">
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <!-- Brand -->
                    <a class="navbar-brand" href="index.html">首頁</a>

                    <!-- Links -->
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">各站資訊</a>
                            <form id="stationPageForm" method="POST" action="stationPage.php">
                                <input name="stationID" id="stationID" type="hidden">
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" onclick="setStationID(1)" style="cursor:pointer;">南港站</a>
                                    <a class="dropdown-item" onclick="setStationID(2)" style="cursor:pointer;">台北站</a>
                                    <a class="dropdown-item" onclick="setStationID(3)" style="cursor:pointer;">板橋站</a>
                                    <a class="dropdown-item" onclick="setStationID(4)" style="cursor:pointer;">桃園站</a>
                                    <a class="dropdown-item" onclick="setStationID(5)" style="cursor:pointer;">新竹站</a>
                                    <a class="dropdown-item" onclick="setStationID(6)" style="cursor:pointer;">苗栗站</a>
                                    <a class="dropdown-item" onclick="setStationID(7)" style="cursor:pointer;">台中站</a>
                                    <a class="dropdown-item" onclick="setStationID(8)" style="cursor:pointer;">彰化站</a>
                                    <a class="dropdown-item" onclick="setStationID(9)" style="cursor:pointer;">雲林站</a>
                                    <a class="dropdown-item" onclick="setStationID(10)" style="cursor:pointer;">嘉義站</a>
                                    <a class="dropdown-item" onclick="setStationID(11)" style="cursor:pointer;">台南站</a>
                                    <a class="dropdown-item" onclick="setStationID(12)" style="cursor:pointer;">左營站</a>
                                </div>
                            </form>
                            <script>
                                function setStationID(stationID) {
                                    document.getElementById('stationID').value = stationID;
                                    document.getElementById('stationPageForm').submit();
                                }
                            </script>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="orderTicket.html">訂票</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        
        
        
        
        <!--content-->
        <div class="container" style="margin-top: 1%;">
            <h1><?php echo $stationName; ?></h1>
            <div class="row">
                <div class="col" id="mapDiv"></div>
                <div class="col">
                    <img src="<?php echo $stationImg; ?>" alt="" width="456" height="342">
                </div>
            </div>
        </div>
        <div class="container" style="margin-top: 1%;">
            <h4 ID="Chart">Google Chart Here</h4>
        </div>
        <script>
             google.charts.load("current", {packages:['corechart']});
             google.charts.setOnLoadCallback(drawChart);
            var map;
            function initMap() {
                map = new google.maps.Map(document.getElementById('mapDiv'), {
                    zoom: 12,
                    center: {lat: <?php echo $stationLat; ?>, lng: <?php echo $stationLng; ?>}
                });
                setMarker();
            }
            function setMarker(){
                var onePos = {lat: parseFloat(<?php echo $stationLat; ?>), lng: parseFloat(<?php echo $stationLng; ?>)};
                var infowindow = new google.maps.InfoWindow({
                    content: "<?php echo $stationAddress; 
                    if($stationAddress=="台灣高雄市左營區高鐵路107號")
                    {
                        echo '<br>大推樺達奶茶!!';
                    }
                    ?>"
                });
                var marker = new google.maps.Marker({
                    map: map,
                    position: onePos,
                    animation: google.maps.Animation.BOUNCE
                });
                marker.addListener('click', function() {
                    infowindow.open(map, marker);
                });
            }
            function chart()
            {
                
                drawChart(<?php echo $chartData; ?>);
            }
            
            
            
            
            
            
            
            function drawChart(inData) {
                console.log(inData);
                console.log(typeof(inData));
                /* 建立一個 google chart 要使用的 data table */
               var chartData = google.visualization.arrayToDataTable([
                    ['年月', '進站人數', { role: 'style' } ],
                    [inData[11][1]+"年"+inData[11][2]+"月",inData[11][0], '#ff6414'],
                    [inData[10][1]+"年"+inData[10][2]+"月",inData[10][0], '#ff6414'],
                    [inData[9][1]+"年"+inData[9][2]+"月",inData[9][0], '#ff6414'],
                    [inData[8][1]+"年"+inData[8][2]+"月",inData[8][0], '#ff6414'],
                    [inData[7][1]+"年"+inData[7][2]+"月",inData[7][0], '#ff6414'],
                    [inData[6][1]+"年"+inData[6][2]+"月",inData[6][0], '#ff6414'],
                    [inData[5][1]+"年"+inData[5][2]+"月",inData[5][0], '#ff6414'],
                    [inData[4][1]+"年"+inData[4][2]+"月",inData[4][0], '#ff6414'],
                    [inData[3][1]+"年"+inData[3][2]+"月",inData[3][0], '#ff6414'],
                    [inData[2][1]+"年"+inData[2][2]+"月",inData[2][0], '#ff6414'],
                    [inData[1][1]+"年"+inData[1][2]+"月",inData[1][0], '#ff6414'],
                    [inData[0][1]+"年"+inData[0][2]+"月",inData[0][0], '#ff6414'],
                ]);
                var view = new google.visualization.DataView(chartData);
                /* 依序建立 data table 中的欄位，必須指定資料型態和欄位名稱 */
//                chartData.addColumn('number', '進站人數');
//                chartData.addColumn('string', 'Year');
//                chartData.addColumn('string', 'Month');
//                chartData.addRows(inData);
                
                /* 設定 google chart 的選項 */
                var options = {
                    title: ' Station - 進站人數 ',
                    width: 956,
                    height: 400,
                    colors:['#ff6414']
                    
                };
                /* 取得要繪製 google chart 的元素 */
                var myChart = new google.visualization.LineChart(document.getElementById('Chart'));
                /* 進行 google chart 的繪製 */
                myChart.draw(view, options);
            }
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCsu91PUX1T-p6inqpnL-XGj26XgD6VUoY&signed_in=true&callback=initMap" async defer></script>
    </body>
</html>