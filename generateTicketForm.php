<?php
$origin= filter_input(INPUT_POST, 'origin');
switch ($origin) {
    case "南港":$originID="0990";break;
    case "台北":$originID="1000";break;
    case "板橋":$originID="1010";break;
    case "桃園":$originID="1020";break;
    case "新竹":$originID="1030";break;
    case "苗栗":$originID="1035";break;
    case "台中":$originID="1040";break;
    case "彰化":$originID="1043";break;
    case "雲林":$originID="1047";break;
    case "嘉義":$originID="1050";break;
    case "台南":$originID="1060";break;
    case "左營":$originID="1070";break;
    default :$originID=$origin;break;
}
$destnation = filter_input(INPUT_POST, 'destnation');
switch ($destnation) {
    case "南港":$destnationID="0990";break;
    case "台北":$destnationID="1000";break;
    case "板橋":$destnationID="1010";break;
    case "桃園":$destnationID="1020";break;
    case "新竹":$destnationID="1030";break;
    case "苗栗":$destnationID="1035";break;
    case "台中":$destnationID="1040";break;
    case "彰化":$destnationID="1043";break;
    case "雲林":$destnationID="1047";break;
    case "嘉義":$destnationID="1050";break;
    case "台南":$destnationID="1060";break;
    case "左營":$destnationID="1070";break;
    default :$destnationID=$destnation;break;
}
$date = filter_input(INPUT_POST, 'date');
$quentity = filter_input(INPUT_POST, 'quentity');
$trainInfo = filter_input(INPUT_POST, 'trainInfo');
$genType = filter_input(INPUT_POST, 'genType');

$url = "http://ptx.transportdata.tw/MOTC/v2/Rail/THSR/DailyTimetable/OD/".$originID."/to/".$destnationID."/".$date."?format=JSON";
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
date_default_timezone_set ("Etc/GMT+0");
$xdate = date("D, d M Y H:i:s").' GMT';
$signature= base64_encode(hash_hmac('sha1', 'x-date: '.$xdate, "PCpRHvDmxU3b5OghMT7fEoFP904", true));
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Accept: application/json',
    'Authorization: hmac username="a6a3d80c99a14c128585a261a93dad46", algorithm="hmac-sha1", headers="x-date", signature="'.$signature.'" ',
    'x-date:'.$xdate
));
$output = json_decode(curl_exec($ch),TRUE);
curl_close($ch);
for ($i=0;$i<count($output);$i++){
    for ($j=$i+1;$j<count($output);$j++){
        $i_time= explode(":", $output[$i]['OriginStopTime']['ArrivalTime']);
        $i_time=$i_time[0]*60+$i_time[1];
        $j_time= explode(":", $output[$j]['OriginStopTime']['ArrivalTime']);
        $j_time=$j_time[0]*60+$j_time[1];
        if($i_time>$j_time){
            $tmp=$output[$i];
            $output[$i]=$output[$j];
            $output[$j]=$tmp;
        }
    }
}
$selectString="";
for ($i=0;$i<count($output);$i++){
    if(($output[$i]['DailyTrainInfo']['TrainNo'].' '.$output[$i]['OriginStopTime']['ArrivalTime'].' '.$output[$i]['DestinationStopTime']['ArrivalTime'])===$trainInfo){
        $selectString.='<option selected>'.$output[$i]['DailyTrainInfo']['TrainNo'].'&#8195;'.$output[$i]['OriginStopTime']['ArrivalTime'].'->'.$output[$i]['DestinationStopTime']['ArrivalTime'].'</option>';
    }
    else{
        $selectString.='<option>'.$output[$i]['DailyTrainInfo']['TrainNo'].'&#8195;'.$output[$i]['OriginStopTime']['ArrivalTime'].'->'.$output[$i]['DestinationStopTime']['ArrivalTime'].'</option>';
    }
}
if(filter_has_var(INPUT_POST, 'renewTrainInfo')){
    echo $selectString;
    exit();
}
?>
<form class="ticketContent" style="margin-bottom: 1%;<?php if($genType=='show'){echo 'display:none;';}?>">
    <div class="form-row">
        <div class="col">
            <select name="origin" class="form-control <?php if($genType=="show"){ echo "allTicketData";} ?>" <?php if($genType=="add"){ echo "id='addOrigin'";} ?> onchange="renewTrainInfo(this)">
                <option value="0990" <?php if($origin==='南港' || $origin==='0990'){echo 'selected';}?>>南港</option>
                <option value="1000" <?php if($origin==='台北' || $origin==='1000'){echo 'selected';}?>>台北</option>
                <option value="1010" <?php if($origin==='板橋' || $origin==='1010'){echo 'selected';}?>>板橋</option>
                <option value="1020" <?php if($origin==='桃園' || $origin==='1020'){echo 'selected';}?>>桃園</option>
                <option value="1030" <?php if($origin==='新竹' || $origin==='1030'){echo 'selected';}?>>新竹</option>
                <option value="1035" <?php if($origin==='苗栗' || $origin==='1035'){echo 'selected';}?>>苗栗</option>
                <option value="1040" <?php if($origin==='台中' || $origin==='1040'){echo 'selected';}?>>台中</option>
                <option value="1043" <?php if($origin==='彰化' || $origin==='1043'){echo 'selected';}?>>彰化</option>
                <option value="1047" <?php if($origin==='雲林' || $origin==='1047'){echo 'selected';}?>>雲林</option>
                <option value="1050" <?php if($origin==='嘉義' || $origin==='1050'){echo 'selected';}?>>嘉義</option>
                <option value="1060" <?php if($origin==='台南' || $origin==='1060'){echo 'selected';}?>>台南</option>
                <option value="1070" <?php if($origin==='左營' || $origin==='1070'){echo 'selected';}?>>左營</option>
            </select>
        </div>
        <div class="col">
            <select name="destnation" class="form-control <?php if($genType=="show"){ echo "allTicketData";} ?>" <?php if($genType=="add"){ echo "id='addDestnation'";} ?> onchange="renewTrainInfo(this)">
                <option value="0990" <?php if($destnation==='南港' || $destnation==='0990'){echo 'selected';}?>>南港</option>
                <option value="1000" <?php if($destnation==='台北' || $destnation==='1000'){echo 'selected';}?>>台北</option>
                <option value="1010" <?php if($destnation==='板橋' || $destnation==='1010'){echo 'selected';}?>>板橋</option>
                <option value="1020" <?php if($destnation==='桃園' || $destnation==='1020'){echo 'selected';}?>>桃園</option>
                <option value="1030" <?php if($destnation==='新竹' || $destnation==='1030'){echo 'selected';}?>>新竹</option>
                <option value="1035" <?php if($destnation==='苗栗' || $destnation==='1035'){echo 'selected';}?>>苗栗</option>
                <option value="1040" <?php if($destnation==='台中' || $destnation==='1040'){echo 'selected';}?>>台中</option>
                <option value="1043" <?php if($destnation==='彰化' || $destnation==='1043'){echo 'selected';}?>>彰化</option>
                <option value="1047" <?php if($destnation==='雲林' || $destnation==='1047'){echo 'selected';}?>>雲林</option>
                <option value="1050" <?php if($destnation==='嘉義' || $destnation==='1050'){echo 'selected';}?>>嘉義</option>
                <option value="1060" <?php if($destnation==='台南' || $destnation==='1060'){echo 'selected';}?>>台南</option>
                <option value="1070" <?php if($destnation==='左營' || $destnation==='1070'){echo 'selected';}?>>左營</option>
            </select>
        </div>
        <div class="col">
            <input name="date" class="form-control <?php if($genType=="show"){ echo "allTicketData";} ?>" type="date" placeholder="日期" value="<?php echo $date; ?>" <?php if($genType=="add"){ echo "id='addDate'";} ?> onchange="renewTrainInfo(this)">
        </div>
        <div class="col">
            <input class="form-control <?php if($genType=="show"){ echo "allTicketData";} ?>" type="number" min="1" placeholder="數量" value="<?php echo $quentity; ?>" <?php if($genType=="add"){ echo "id='addQuentity'";} ?>>
        </div>
        <div class="col">
            <select class="form-control <?php if($genType=="show"){ echo "allTicketData";} ?>" <?php if($genType=="add"){ echo "id='addTrainInfo'";} ?>>
                <?php echo $selectString; ?>
            </select>
        </div>
        <?php
        if($genType=='show'){
            echo '<div class="col">';
            echo '<button type="button" class="form-control btn btn-danger" onclick="deleteTicket(this)">刪除</button>';
            echo '</div>';
        }
        ?>
    </div>
    <?php
    if($genType=='add'){//true means add not show
        echo '<button type="button" class="btn btn-info" style="float: right;" onclick="addTicket()">新增</button>';
    }
    ?>
</form>