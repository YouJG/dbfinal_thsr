/*overlay*/
function on() {
    $('#overlay').css('display', 'block');
    timetableQuery();
}

function off() {
    $('#overlay').css('display', 'none');
    $('#overlay').css('background-image', 'url("loading_spinner.gif")');
    $('#text').css('overflow-y', '');
    $('#text').css('overflow-x', '');
    $('#text').css('background-color', '');
    $('#text').html('');
}

/*dynamic ticket field*/
function generateTicketForm(origin,destnation,date,quentity,trainInfo,genType){
    var result;
    $.ajax({
       url: "generateTicketForm.php",
       method: "post",
       data:{
           origin:origin,
           destnation:destnation,
           date:date,
           quentity:quentity,
           trainInfo:trainInfo,
           genType:genType
       },
       async: false,
       success:function(data){
           result=data;
       }
    });
    return result;
}
function showTicketContent(clickedButton){
    $(clickedButton).next().slideToggle("fast");
}
function addTicket(){
    var origin=$("#addOrigin").val();
    var destnation=$("#addDestnation").val();
    var date=$("#addDate").val();
    var quentity=$("#addQuentity").val();
    var trainInfo=$("#addTrainInfo").val()+"";
    var buttonHTML=date+" "+(trainInfo.split(" ")[0]);
    
    var newTicket="";
    var returnTemplate = generateTicketForm(origin,destnation,date,quentity,trainInfo.replace(" "," ").replace("->"," "),"show");
    newTicket+="<button class='btn btn-secondary' onclick='showTicketContent(this)' style='display: block;margin-bottom:5px;'>"+buttonHTML+"</button>";
    newTicket+=returnTemplate;
    $("#ticketForm").append(newTicket);
}
function deleteTicket(select){
    var button = $(select).parent().parent().parent().prev();
    var form = $(select).parent().parent().parent();
    $(button).remove();
    $(form).remove();
}
function renewTrainInfo(select){
    var trainInfoTag;
    var origin_val;
    var destnation_val;
    var date_val;
    if($(select).attr('name')==='origin'){
        trainInfoTag=$(select).parent().next().next().next().next().children();
        origin_val=$(select).val();
        destnation_val=$(select).parent().next().children().val();
        date_val=$(select).parent().next().next().children().val();
    }
    else if($(select).attr('name')==='destnation'){
        trainInfoTag=$(select).parent().next().next().next().children();
        origin_val=$(select).parent().prev().children().val();
        destnation_val=$(select).val();
        date_val=$(select).parent().next().children().val();
    }
    else if($(select).attr('name')==='date'){
        trainInfoTag=$(select).parent().next().next().children();
        origin_val=$(select).parent().prev().prev().children().val();
        destnation_val=$(select).parent().prev().children().val();
        date_val=$(select).val();
    }
    $(trainInfoTag).html("<option selected>renew now, please wait.</option>");

    $.ajax({
       url: "generateTicketForm.php",
       method: "post",
       data:{
           origin:origin_val,
           destnation:destnation_val,
           date:date_val,
           renewTrainInfo:"rmjgdseauhgdey"
       },
       async: true,
       success:function(data){
           $(trainInfoTag).html(data);
       }
    });
}
function showForm() {
    if ($("#taiwanIDbutton").html() === "提交") {
        $("#taiwanID").attr("disabled", "disabled");
        $("#taiwanIDbutton").html("重新提交");

        var formString = "<hr>";
        formString += "<p class='h2'>您的訂票</p>";
        formString += "<br>";
        var id = $("#taiwanID").val();
        $.ajax({
            url: "orderTicket.php",
            method: "post",
            data: {
                id: id
            },
            dataType: "json",
            async: false,
            success: function (data) {
                if (data !== null) {
                    for(i=0;i<data.length;i++){
                        var trainInfo = data[i][5]+" "+data[i][6]+" "+data[i][7];
                        var returnTemplate = generateTicketForm(data[i][1],data[i][2],data[i][3],data[i][4],trainInfo,"show");
                        formString+="<button class='btn btn-secondary' onclick='showTicketContent(this)' style='display: block;margin-bottom:5px;'>"+data[i][3]+" "+data[i][5]+"</button>";
                        formString+=returnTemplate;
                    }
                }
            }
        });
        $("#ticketForm").append(formString);
        
        var year = new Date().getFullYear();
        var month = new Date().getMonth() + 1;
        if ((month + "").length === 1) {
            month = "0" + month;
        }
        var date = new Date().getDate();
        if ((date + "").length === 1) {
            date = "0" + date;
        }
        date = year + "-" + month + "-" + date;
        var result = generateTicketForm("南港","台北",date,"1","","add");
        $("#addTicketForm").html(result);
        
        $("#confirmAllModify").html('<button type="button" class="btn btn-success btn-block" onclick="confirmAllModify(this)">更新資料</button>');
    } else if ($("#taiwanIDbutton").html() === "重新提交") {
        $("#taiwanID").removeAttr("disabled");
        $("#taiwanIDbutton").html("提交");
        $("#ticketForm").html("");
        $("#addTicketForm").html("");
        $("#confirmAllModify").html("");
    }

}



/*timetable query*/
$(document).ready(function () {
    var year = new Date().getFullYear();
    var month = new Date().getMonth() + 1;
    if ((month + "").length === 1) {
        month = "0" + month;
    }
    var date = new Date().getDate();
    if ((date + "").length === 1) {
        date = "0" + date;
    }
    $('.date').val(year + "-" + month + "-" + date);
});
function timetableQuery() {
    var original = document.getElementById('original').value;
    var destnation = document.getElementById('destnation').value;
    var date = document.getElementById('date').value;
    var time = document.getElementById('time').value;
    $.ajax({
        url: "getTimetable.php",
        method: "POST",
        async: true,
        dataType: 'json',
        data: {
            'original': original,
            'destnation': destnation,
            'date': date,
            'time': time
        },
        success: function (data) {
            var timetable = data;
            $('#overlay').css('background-image', 'url()');
            $('#text').css('overflow-y', 'scroll');
            $('#text').css('overflow-x', 'scroll');
            $('#text').css('background-color', 'white');
            $('#text').scrollTop(0);
            var table = "";
            table += "<table class='table table-striped'>";
            table += "<thead>";
            table += "<tr>";
            table += "<th scope='col'>車次</th><th scope='col'>出發時間</th><th scope='col'>抵達時間</th>";
            table += "</tr>";
            table += "</thead>";
            table += "<tbody>";
            for (i = 0; i < timetable.length; i++) {
                table += "<tr>";
                table += "<td>" + timetable[i]['DailyTrainInfo']['TrainNo'] + "</td>";
                table += "<td>" + timetable[i]['OriginStopTime']['ArrivalTime'] + "</td>";
                table += "<td>" + timetable[i]['DestinationStopTime']['ArrivalTime'] + "</td>";
                table += "</tr>";
            }
            table += "</tbody>";
            table += "</table>";
            $('#text').html(table);
        }
    });
}