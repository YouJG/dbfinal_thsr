<?php
//header("Content-Type:text/html; charset=utf-8");

$original= filter_input(INPUT_POST,'original');
$destnation= filter_input(INPUT_POST,'destnation');
$date= filter_input(INPUT_POST, 'date');
$time= filter_input(INPUT_POST, 'time');

/*$type= filter_input(INPUT_POST, 'type');*/

//$original='0990';
//$destnation='1000';
//$date='2018-06-06';
//$time=990;

$url = "http://ptx.transportdata.tw/MOTC/v2/Rail/THSR/DailyTimetable/OD/".$original."/to/".$destnation."/".$date."?format=JSON";
//echo $url;
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
date_default_timezone_set ("Etc/GMT+0");
$xdate = date("D, d M Y H:i:s").' GMT';
$signature= base64_encode(hash_hmac('sha1', 'x-date: '.$xdate, "PCpRHvDmxU3b5OghMT7fEoFP904", true));
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Accept: application/json',
    'Authorization: hmac username="a6a3d80c99a14c128585a261a93dad46", algorithm="hmac-sha1", headers="x-date", signature="'.$signature.'" ',
    'x-date:'.$xdate
));
$output = json_decode(curl_exec($ch),TRUE);
curl_close($ch);

//sort
$resultOutput=[];
for ($i=0;$i<count($output);$i++){
    for ($j=$i+1;$j<count($output);$j++){
        $i_time= explode(":", $output[$i]['OriginStopTime']['ArrivalTime']);
        $i_time=$i_time[0]*60+$i_time[1];
        $j_time= explode(":", $output[$j]['OriginStopTime']['ArrivalTime']);
        $j_time=$j_time[0]*60+$j_time[1];
        if($i_time>$j_time){
            $tmp=$output[$i];
            $output[$i]=$output[$j];
            $output[$j]=$tmp;
        }
    }
}

//start from arrive time to final
for ($i=0;$i<count($output);$i++){
    $i_time= explode(":", $output[$i]['OriginStopTime']['ArrivalTime']);
    $i_time=$i_time[0]*60+$i_time[1];
    if($i_time>=$time)
        array_push($resultOutput, $output[$i]);
}
echo json_encode($resultOutput);
?>


